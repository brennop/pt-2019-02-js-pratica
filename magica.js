const table = document.querySelectorAll("#minha-linda-tabela > tbody")[1]
  .children;
const tableContent = Array.from(table).filter(
  item => item.childElementCount == 4
);

const totalRow = document.getElementById("tabela-total");
let total = 0;

tableContent.forEach(item => {
  item.classList.add(
    item.children[3].innerText > 0 ? "table-success" : "table-danger"
  );
  total += Number(item.children[3].innerText);
});

totalRow.children[1].innerText = total.toFixed(2);

const darkBtn = document.querySelector(".btn");
darkBtn.onclick = () => {
  const darkElements = document.querySelectorAll(
    ".bg-light, .bg-dark, .text-light, .text-dark, btn-dark, btn-light"
  );
  darkElements.forEach(element => {
    Array.from(element.classList)
      .filter(name => name.includes("light") || name.includes("dark"))
      .forEach(className => {
        type = className.split("-")[0];
        element.classList.toggle(type + "-light");
        element.classList.toggle(type + "-dark");
      });
  });
};
